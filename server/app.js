var express     = require ('express');
var bodyParser  = require('body-parser');
var movieCtrl  = require('./api/movie/movie.controller');
var genreCtrl   = require('./api/genre/genre.controller');
var userCtrl = require('./api/user/user.controller');

var app  = express();
var cors = require('cors');

app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(cors());

app.get ("/api/movies", userCtrl.verifyToken, movieCtrl.list); 
app.post("/api/movies", userCtrl.verifyToken, movieCtrl.add);
app.delete("/api/movies/:movie_id", userCtrl.verifyToken, movieCtrl.remove);
app.get ("/api/genres", userCtrl.verifyToken, genreCtrl.list);
app.post("/api/genres", userCtrl.verifyToken, genreCtrl.add);
app.delete("/api/genres/:genre_id", userCtrl.verifyToken, genreCtrl.remove);

app.post ("/api/login"  , userCtrl.login)

app.use(function (req, resp) {
    resp.status(440);
    resp.send("Error File not Found");
});

// set port and start webserver
app.listen('3000', function () {
    console.log("Server running at http://localhost:3000");
});
