var movieList = [
    { movie_id: 1, title: 'Avengers: Endgame', actors:'Robert Downey Jr.', studio: 'Marvel', date: '2019', genre_id: '1', genre: 'Action', imageurl: 'https'},
    { movie_id: 2, title: 'Toy Story 4', actors:'Tom Hanks, Rashida Jones', studio: 'Disney', date: '2019', genre_id: '2', genre: 'Adventure', imageurl: 'https'},
    { movie_id: 3, title: 'Isnt it romatic', actors:'Rebel Wilson, Liam Hemsworth', studio: 'Warners Bros', date: '2019', genre_id: '3', genre: 'Comedy', imageurl: 'https'},
    { movie_id: 4, title: 'Aladdin', actors:'Will Smith, Mena Massoud', studio: 'Disney', date: '2019', genre_id: '4', genre: 'Fantasy', imageurl: 'https'},
    { movie_id: 5, title: 'Brightburn', actors:'Elizabeth Banks, David Denman', studio: 'Screen Gems', date: '2019', genre_id: '5', genre: 'Horror', imageurl: 'https'},
];

var current_movie_id = 6;

exports.list = function (req, resp) {
    resp.status(200)
    resp.type("application/json");
    resp.json(movieList);
}

exports.listone = function (req, resp) {
    if (!req.params.movie_id) {
        handleErr(resp);
    } else {
        var found = false;
        var movie_id = req.params.movie_id;

        for (var i=0; i<movieList.length; i++) {
            if (movieList[i].movie_id == movie_id) {
                movieList.splice(i,1);
                found = true;
                break;
            }
        }
        resp.json(movieList);
    }
}

exports.add = function(req, resp) {
    if (!req.body.info) {
         handleErr(resp);
     } else {
        var newinfo = req.body.info;

        newinfo.movie_id = current_movie_id++;
        movieList.push(newinfo);
        resp.status(200)
        resp.type("application/json");
        resp.json(newinfo);
    }
}

exports.remove = function (req, resp) {
    if (!req.params.movie_id) {
        handleErr(resp);
    } else {
        var found = false;
        var movie_id = req.params.movie_id;

        for (var i=0; i<movieList.length; i++) {
            if (movieList[i].movie_id == movie_id) {
                movieList.splice(i,1);
                found = true;
                break;
            }
        }
        resp.status(200).json({success: found});
    }
}

// Error handling 
function handleErr(res) {
    handleErr(res, null);
}

function handleErr(res, err) {
    console.log(err);
    res.status(500).json({error: true});
}